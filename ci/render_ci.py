#!/usr/bin/env python
"""This script render gitlab ci yaml files from jinja2 templates
"""

from os.path import join, realpath, pardir, dirname
from os import walk, path
import yaml
from jinja2 import Environment, FileSystemLoader
from pprint import pprint
from fnmatch import fnmatch

PROJECT_DIR = realpath(join(dirname(realpath(__file__)), pardir))
CI_DIR = join(PROJECT_DIR, 'ci')
CI_TEMPLATES_DIR = join(CI_DIR, 'templates')
CI_CONFIGURATION_DIR = join(CI_DIR, 'config')
CI_PIPELINES_DIR = join(CI_DIR, 'pipelines')

def load_config(config):
    """Load single yaml file as configuration dictionary
    :param `config`: path to yaml configuration file
    :type arg: str
    :return: configuration dictionary
    """
    if fnmatch(config, '*.yml') or fnmatch(config, '*.yaml'):
        with open(config, 'r') as yaml_config:
            return yaml.safe_load(yaml_config)


def load_config_dir(config_dir):
    """Load configuration yaml files from config_dir
    :param `config_dir`: directory with yaml files
    :type arg: str
    :return: configuration dictionary
    """
    result = None
    for root, _, files in walk(config_dir):
        for config in files:
            config_path = join(root, config)
            if result is None:
                result = load_config(config_path)
            else:
                for key, val in load_config(config_path).items():
                    result[key] = val
    return result


def render_template(jinja_env, filename, vars_dict):
    """Render template from templates directory.
    :param `jinja_env`: jinja2 environment object.
    :param `filename`: path to jinja2 template.
    :type arg: str
    :param `vars_dict`: render configuration dictionary.
    :type arg: dict
    :return: json manifest
    """
    template = jinja_env.get_template(filename)
    rendered_template = template.render(vars_dict)
    return yaml.safe_load(rendered_template)


def main():
    """Main function to run renders.
    """
    ci_config = load_config_dir(CI_CONFIGURATION_DIR)
    jinja2_env = Environment(loader=FileSystemLoader(CI_TEMPLATES_DIR))
    for template in jinja2_env.list_templates():
        rendered_template = render_template(
            jinja2_env,
            template,
            ci_config
            )
        ci_file, _ = path.splitext(template)
        with open(join(CI_PIPELINES_DIR, ci_file), 'w') as file:
            yaml.dump(rendered_template, file, default_flow_style=False)


main()
